# AT&amp;T_Code_Challenge
This challenge seeks to get all at&t related issues from GitHub. Pagination is one of the challenges which makes a huge data stream. Get all issues within abuse limit which is the restrictions of sending requests in GitHub, is another limitation.
Promise was major used to handle asynchronies methods in Node.Js, while async and await was used in some test cases.

Here are some introductions.

## Tech Stack

Dependencies: Node.js + Github API +axios.js+Express.js+

Dev Dependencies: Jest.js

## Restful API Spec

```json
API: GET /issues
Result:
[
  {
  	id,
    title,
    body,
    url,
    user [
    	{
    		id,
    		login,
    		url
    		...
    			
    	}
    ]
    comments [
      {
        id,
        url,
        body
        ...
  	  },
  	  ...
    ]
  },
  ...
]
```

## Quick Start

Unit test

```shell
npm test
```

Local Development

```shell
npm i
npm start
```


