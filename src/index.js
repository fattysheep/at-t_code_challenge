const express = require("express");
const https = require("https");
const bodyParser = require("body-parser")
const issuesRouter = require("./server/routes/issues");

const app = new express();
const PORT = process.env.PORT || 8888;

app.use(bodyParser.json());

app.use('/', issuesRouter);

app.listen(process.env.PORT || 8888);
console.log(`Sever listening on port ${PORT}`);

module.exports = app;