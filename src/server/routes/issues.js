const express = require("express");
const router = express.Router();
const issues = require("../service/controller");

router.get("/issues",  (req, res) =>{
	issues.getIssues(res);
});

module.exports = router;