const axios = require('axios');
const parseLinkHeader = require('parse-link-header');
const _ = require('lodash');
const properties = require('../../../property.json');

axios.defaults.headers.common['Authorization'] = properties['auth']['token'];
axios.defaults.headers.common['User-Agent'] = properties['auth']['test'];
axios.interceptors.response.use( (response)=> {
  return response;
},  (err)=> {
  console.log(err);
});

let PER_PAGE = properties['pageParameter']['PER_PAGE'];
let CONCURRENCY = properties['pageParameter']['CONCURRENCY'];
let rootURL = properties['URL']['rootUrl'];

function getIssues(res) {
  let repo_url = properties['URL']['repoUrl'];
  let repos = [];
  let issues = [];
  let comments = [];
  getRequestTotalPages(repo_url)// get number of pages for repos
    .then( (pages) => {// get all repos
      return hanldeArrayOfPromises(generatePagedRequests(repo_url, pages));
    }, (err) =>  {
      console.log(err);
    })
    .then(function () {// get number of pages for issues
      repos = _.flatten(parseResponse(arguments));
      return getAttributesPage(repos, '/issues');
    }, (err) =>  {
      console.log(err);
    })
    .then(function () {// get all issues    
      let repoIssuesPageArray = _.flatten(Array.prototype.slice.call(arguments));
      return getAttributes(repos, repoIssuesPageArray, '/issues');
    },(err) =>  {
      console.log(err);
    })
    .then(function () { // get number of pages for comments
      issues = _.flatten(parseResponse(arguments));
      return getAttributesPage(repos, '/issues/comments');
    }, (err) =>  {
      console.log(err);
    })
    .then(function () {// get all comments    
      let repoCommentsPageArray = _.flatten(Array.prototype.slice.call(arguments));
      return getAttributes(repos, repoCommentsPageArray, '/issues/comments');
    }, (err) =>  {
      console.log(err);
    })
    .then(function () {//mapping comments to issues
      comments = _.flatten(parseResponse(arguments));
      issues = issues.map( (issue)=> {
        let newComments = [];
        if (issue.comments) {
          comments.forEach( (comment) =>{
            if (comment.issue_url == issue.url) {
              newComments.push(comment);
            }
          });
        }
        return {
          id: issue.id,
          url: issue.url,
          title: issue.title,
          user: issue.user,
          body: issue.body,
          comments: newComments
        }
      });
      res.send(issues);
    });
}

function getAttributesPage(repos, attribute_url) {
  let promises = [];
  repos.forEach( (repo) =>{
    if (repo.open_issues_count) {
      let newUrl = `${rootURL}${repo.name}${attribute_url}`;
      promises.push(getRequestTotalPages(newUrl));
    }
  })
  return hanldeArrayOfPromises(promises);
}

function getAttributes(repos, repoPageArray, attribute_url) {
  let promises = [];
  let i = 0;
  repos.forEach( (repo)=> {
    if (repo.open_issues_count) {
      let newUrl = `${rootURL}${repo.name}${attribute_url}`;

      promises.push(generatePagedRequests(newUrl, repoPageArray[i]));
      i += 1;
    }
  });
  promises = _.flatten(promises);

  return hanldeArrayOfPromises(promises);
}

function generatePagedRequests(url, pages) {
  return _.range(pages).map( (i) => {
    return axios.get(`${url}?page=${i + 1}&per_page=${PER_PAGE}`);
  });
}

function parseResponse(response) {
  return _.flatten(Array.prototype.slice.call(response)).map( (res) =>{
    return res ? res.data : [];
  });
}

function getRequestTotalPages(url) {
  return new Promise( (resolve, reject) =>{
    axios.get(`${url}?per_page=${PER_PAGE}`)
      .then( (res)=> {

        if (res && res.headers) {
          let links = parseLinkHeader(res.headers.link);
          let pages = links ? (parseInt(links.last.page) || 1) : 1;
          resolve(pages);
        } else {
          resolve(1);
        }
      });
  });
}

function hanldeArrayOfPromises(promises) {
  if (promises.length <= CONCURRENCY) {
    return axios.all(promises);
  } else {
    return new Promise( (resolve, reject) =>{
      handlePromises(promises, 0, resolve, []);
    });
  }
}

function handlePromises(promises, handled, resolve, result) {
  if (promises.length === handled) {
    resolve(result);
    return;
  }
  const end = Math.min(handled + CONCURRENCY, promises.length);
  axios.all(promises.slice(handled, end))
    .then( (res)=> {
      result = result.concat(res);
      handlePromises(promises, end, resolve, result);
    });
}

module.exports = { 
  getIssues, 
  getAttributesPage, 
  getRequestTotalPages, 
  generatePagedRequests, 
  hanldeArrayOfPromises,
  getAttributesPage,
  parseResponse,
  getAttributes
  };