
const server = require('../src/index');

describe("server",  () => {
  it("server exists.",  (done) => {
    expect(server).toBeDefined();
    done();
  });
  it("server is listening.",  (done) => {
    expect(server.listen).toBeDefined();
    done();
  });
});
