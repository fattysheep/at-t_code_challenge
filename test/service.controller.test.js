
const controller = require('../src/server/service/controller');
const _ = require('lodash');
const properties = require('../property.json');
const testProperties = require('./testProperty.json');
let repo_url = properties['URL']['repoUrl'];
const axios = require('axios');

axios.defaults.headers.common['Authorization'] = properties['auth']['token'];
axios.defaults.headers.common['User-Agent'] = properties['auth']['test'];
axios.interceptors.response.use( (response)=> {
  return response;
},  (err)=> {
  console.log(err);
});

describe("issues controller",  ()=> {
  it("getAttIssues should exist.",  () => {
    expect(controller).toBeDefined();
  });
});

describe("Get Total Pages For All Repos",  ()=> {
  it("Has at least 1 page",  async (done) => {
    let pages = await controller.getRequestTotalPages(repo_url);
    expect(pages).toBeGreaterThan(0);
    done();
  });
});

  describe("Get All Repos on Page 1",  ()=> {
    it("Has at least 1 Repo",  async (done) => {
      let repos = await controller.hanldeArrayOfPromises(controller.generatePagedRequests(repo_url, 1));
      expect(repos.length).toBeGreaterThan(0);
      done();
    });
  });


  describe("Get All Issue  Pages  ",  ()=> {
    it("Has at least 1 page",  async (done) => {
      let repos = await controller.hanldeArrayOfPromises(controller.generatePagedRequests(repo_url, 1));
      let issuePage = await controller.getAttributesPage( _.flatten(controller.parseResponse(repos)));
      expect(issuePage.length).toBeGreaterThan(0);
      done();
    });
  });


  describe("Get All issues in Repo 1",  ()=> {
    it("Has at least 1 open issue",   async (done) => {
      repos = [];
      repoData = await axios.get(testProperties['demoRepoUrl']);
      expect(repoData.data.open_issues_count).toBeTruthy();
      repos.push(repoData.data);
      issues = await controller.getAttributes(repos, testProperties['repoIssuesPageArray'], '/issues');
      expect(issues.length).toBeGreaterThan(0);
      done();   
    });
  });


